package ai.easy.robot.annotation

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

/**
 * 消息类型
 */
@Retention(RetentionPolicy.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class MessageType(val value: String = "")
