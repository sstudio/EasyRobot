package ai.easy.robot

import ai.easy.robot.annotation.MessageType
import com.google.auto.service.AutoService
import com.squareup.javapoet.JavaFile
import java.io.IOException
import javax.annotation.processing.*
import javax.lang.model.SourceVersion
import javax.lang.model.element.Modifier.ABSTRACT
import javax.lang.model.element.Modifier.PUBLIC
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.tools.Diagnostic.Kind.ERROR
import javax.tools.Diagnostic.Kind.WARNING

/**
 * 代码处理器
 */
@AutoService(Processor::class)
class EasyMsgProcessor : AbstractProcessor() {

    private lateinit var messager: Messager
    private var ANNOTATION = MessageType::class.java

    @Synchronized
    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        messager = processingEnv.messager
    }

    override fun getSupportedAnnotationTypes(): Set<String> {
        return setOf(MessageType::class.java.canonicalName)
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latestSupported()
    }

    override fun process(annotations: Set<TypeElement>, roundEnv: RoundEnvironment): Boolean {
        //messager.printMessage(WARNING,">>>123")

        val typeElements =
                roundEnv.getElementsAnnotatedWith(MessageType::class.java)
                        // Filter annotated classes which is defined
                        // with @Target(AnnotationTarget.CLASS)
                        .filterIsInstance<TypeElement>()
                        // Filer annotated classes which is public and is not abstract
                        .filter(::isValidClass)
                        .map(::buildAnnotatedClass)

        //Generate JsonUtil Class using JavaPoet.
        generateMsgUtilClass(typeElements)

        return true
    }

    private fun isValidClass(annotatedClass: TypeElement): Boolean {

        // if class is not a public class
        if (!annotatedClass.modifiers.contains(PUBLIC)) {
            val message = String.format("Classes annotated with %s must be public.",
                    "@" + ANNOTATION.simpleName)
            println("Classes annotated with %s must be public.")
            messager.printMessage(ERROR, message, annotatedClass)
            return false
        }

        // if the class is a abstract class
        if (annotatedClass.modifiers.contains(ABSTRACT)) {
            val message = String.format("Classes annotated with %s must not be abstract.",
                    "@" + ANNOTATION.simpleName)
            println("Classes annotated with %s must not be abstract.")

            messager.printMessage(ERROR, message, annotatedClass)
            return false
        }

        return true
    }

    private fun buildAnnotatedClass(typeElement: TypeElement): EasyMsgClass {

        val variableNames = typeElement.enclosedElements
                .filterIsInstance<VariableElement>()
                .map { Pair(it.simpleName.toString(), it.asType()) }

        return EasyMsgClass(typeElement, variableNames)
    }

    private inline fun note(str: String) {
        messager.printMessage(WARNING, str)
    }

    @Throws(/*ClassPackageNotFoundException::class,*/ IOException::class)
    private fun generateMsgUtilClass(easyClasses: List<EasyMsgClass>) {
//        if (easyClasses.isEmpty()) {
//            return
//        }
        easyClasses.forEach {
            note(">>process class ${it.type}")
            val packageName = Utils.getPackageName(processingEnv.elementUtils, it.typeElement)
            val generatedClass = MsgConvertorCodeGenerator.generateClass(it)
            val javaFile = JavaFile.builder(packageName, generatedClass).build()
            javaFile.writeTo(processingEnv.filer)
        }
    }
}

