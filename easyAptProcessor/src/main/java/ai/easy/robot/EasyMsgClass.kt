package ai.easy.robot

import javax.lang.model.element.TypeElement
import javax.lang.model.type.TypeMirror

internal class EasyMsgClass(val typeElement: TypeElement, val variableNames: List<Pair<String, TypeMirror>>) {
    val type: TypeMirror
        get() = typeElement.asType()
}
