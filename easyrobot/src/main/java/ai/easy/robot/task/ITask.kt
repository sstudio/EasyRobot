package ai.easy.robot.task

import com.easy.robot.task.TaskContext

/**
 * 机器人任务接口，由于处理其行为逻辑
 */
interface ITask {

    /**
     * 任務名稱
     *
     * @return
     */
    val name: String

    /**
     * 任务正在运行
     *
     * @return
     */
    val isAlive: Boolean

    /**
     * 任务是否可以恢复
     *
     * @return
     */
    fun canResume(): Boolean

    /**
     * 任务开始时触发
     *
     * @param ctx
     */
    fun onStart(ctx: TaskContext)

    /**
     * 任务结束时触发
     *
     * @param ctx
     */
    fun onStop(ctx: TaskContext)

    /**
     * 任务暂停时触发
     *
     * @param ctx
     */
    fun onPause(ctx: TaskContext)

    /**
     * 任务恢复时触发
     *
     * @param ctx
     */
    fun onResume(ctx: TaskContext)

}
