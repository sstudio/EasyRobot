package ai.easy.robot.task

/**
 * 询问用户是否打断任务的通知器
 */
abstract class InterruptComingSelector {

    companion object {
        const val CHOICE_STOP = 1
        const val CHOICE_PAUSE = 2
        const val CHOICE_DO_NOTHING = 0
    }

    /**
     * 开始询问用户时
     * @param taskName  当前任务名
     * @param canResume 当前任务可恢复
     */
    abstract fun makeSelection(taskName: String, canResume: Boolean)

    /**
     * 当前任务已结束或者用户取消选择时
     */
    abstract fun onFinishOrCancel()

    internal var isFinished: Boolean = false
    internal var userChoice: Int = -1

    /**
     * 设置用户的选择结果
     * @param choice
     * @see CHOICE_STOP
     * @see CHOICE_PAUSE
     * @see CHOICE_DO_NOTHING
     */
    fun finish(choice: Int) {
        userChoice = choice
        isFinished = true
    }
}