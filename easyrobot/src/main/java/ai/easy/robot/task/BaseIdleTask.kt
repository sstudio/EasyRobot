package com.easy.robot.task

import ai.easy.robot.task.ITask
import ai.easy.robot.utils.logd


open class BaseIdleTask : ITask {

    override val isAlive: Boolean = false

    override fun canResume(): Boolean = false

    override val name: String = "Idle"

    override fun onStart(ctx: TaskContext) {
        logd("Idle task start")
    }

    override fun onStop(ctx: TaskContext) {
        logd("Idle task stop")
    }

    override fun onPause(ctx: TaskContext) {}

    override fun onResume(ctx: TaskContext) {}
}