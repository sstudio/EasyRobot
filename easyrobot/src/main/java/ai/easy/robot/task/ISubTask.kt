package ai.easy.robot.task

/**
 * 子任务接口
 */
interface ISubTask {
    /**
     * 设置执行任务超时，一旦超时，将结束子任务run()并触发onCancel(), 单位: ms
     */
    fun timeout(): Int

    /**
     * 子任务的主要工作
     *
     * @return 决定下个子任务是否继续
     */
    fun run(paraMgr: SubTaskParallelManager): Boolean

    /**
     * 子任务正常结束或者运行超时时触发
     *
     * @param isTimeout
     * @return 决定下个子任务是否继续
     */
    fun onCancel(isTimeout: Boolean): Boolean

}
