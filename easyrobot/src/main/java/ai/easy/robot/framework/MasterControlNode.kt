package com.easy.robot.framework

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.os.Messenger

/**
 * 负责管理节点的特殊节点
 */
class MasterControlNode constructor(connector: MasterConnector) : BaseNode("/mastercntl", connector) {

    override fun onMessageReceive(topic: String, data: Bundle) {
        //TODO
    }

    fun listNodes(onAnswer: (Array<*>) -> Unit) {
        val m = Message.obtain()
        m.what = MasterService.NODE_MGR_LIST
        m.replyTo = Messenger(object : Handler() {
            override fun handleMessage(msg: Message?) {
                val a = msg?.obj
                if (a is Array<*>) {
                    onAnswer(a)
                }
            }
        })
        connector.messenger?.send(m)
    }

    fun clearAll() {
        val m = Message.obtain()
        m.what = MasterService.NODE_MGR_CLR
        connector.messenger?.send(m)
    }
}