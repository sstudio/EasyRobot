package ai.easy.robot.utils

import android.util.Log

internal inline fun Any.logd(str: String, tag: String? = null) {
    Log.i(tag ?: "${this::class.java.simpleName}", str)
}

internal fun Any.loge(str: String, tag: String? = null) {
    Log.e(tag ?: this::class.java.simpleName, str)
}

internal fun Any.logi(str: String, tag: String? = null) {
    Log.i(tag ?: this::class.java.simpleName, str)
}