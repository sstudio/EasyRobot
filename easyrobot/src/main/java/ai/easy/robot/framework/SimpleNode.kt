package com.easy.robot.framework

import android.os.Bundle

class SimpleNode constructor(name: String, connector: MasterConnector) : BaseNode(name, connector) {

    constructor(name: String, connector: MasterConnector, initor: (SimpleNode.() -> Unit?) = {}) : this(name, connector) {
        initor()
    }

    override fun onMessageReceive(topic: String, data: Bundle) {
        onMessage?.invoke(topic, data)
    }

    var onMessage: ((String, Bundle) -> Unit)? = null
    fun onMessage(f: (String, Bundle) -> Unit = { t, m -> }) {
        onMessage = f
    }

    override fun onNodeRegisted() {
        super.onNodeRegisted()
        onRegister?.invoke()
    }

    var onRegister: (() -> Unit)? = null
    fun onRegister(f: () -> Unit = {}) {
        onRegister = f
    }
}