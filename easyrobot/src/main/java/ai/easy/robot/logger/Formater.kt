package ai.easy.robot.logger

import java.text.SimpleDateFormat
import java.util.*

//
fun Date.dateFormat(): String = SimpleDateFormat("yyyy/MM/dd hh:mm:ss").format(this)

fun Date.toHourMin(): String = SimpleDateFormat("hh:mm").format(this)

fun Date.toHourMinSec(): String = SimpleDateFormat("hh:mm:ss").format(this)
