package ai.easy.robot.user

import ai.easy.robot.annotation.MessageType


@MessageType("")
data class MsgText(@JvmField var text: String = "")

@MessageType("")
data class MsgMotor(@JvmField var L: Int = 0, @JvmField var R: Int = 0)

@MessageType("")
data class MsgSpeakAI(@JvmField var text: String = "", @JvmField var json: String = "")