package ai.easy.robot.user

import android.app.Activity
import android.app.Application
import com.easy.robot.framework.MasterConnector
import com.easy.robot.framework.MasterService
import com.easy.robot.task.TaskPool

open class RobotApplication : Application() {

    val pool = TaskPool()

    val connector: MasterConnector by lazy { MasterConnector(this) }

    fun initMasterConnector(activity: Activity, onReady: (Boolean) -> Unit) {
        if (/*this::connector.isInitialized && */connector.isConnected) {
            onReady(false)
        } else {

//            startService(Intent().apply {
//                setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                setClass(this@RobotApplication, MasterService.javaClass)
//            })
            //
            connector.onConnect { onReady(true) }
            connector.connect(MasterService::class.java)
        }
    }

    fun releaseMasterConnector() {
        connector.disconnect()
    }

    override fun onCreate() {
        super.onCreate()
        //
    }

}