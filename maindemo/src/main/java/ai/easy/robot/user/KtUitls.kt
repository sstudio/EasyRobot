package ai.easy.robot.user

import android.util.Log
import com.google.gson.GsonBuilder

fun Any.logd(str: String, tag: String? = null) {
    Log.i(tag ?: "${this::class.java.simpleName}", str)
}

fun Any.loge(str: String, tag: String? = null) {
    Log.e(tag ?: this::class.java.simpleName, str)
}

fun Any.logi(str: String, tag: String? = null) {
    Log.i(tag ?: this::class.java.simpleName, str)
}


inline fun <reified T> parseJson(str: String): T? {
    return GsonBuilder().serializeNulls().create().fromJson(str, T::class.java)
}


