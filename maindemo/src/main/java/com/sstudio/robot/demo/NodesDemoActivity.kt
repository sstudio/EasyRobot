package com.sstudio.robot.demo

//import org.jetbrains.anko.sdk25.coroutines.onClick

import ai.easy.robot.user.ConvertMsgText
import ai.easy.robot.user.MsgText
import ai.easy.robot.user.logd
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.easy.robot.framework.SimpleNode
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

/**
 * 演示节点的添加
 */
class NodesDemoActivity : BaseActivity() {

    override fun createView(): View? = UI {
        verticalLayout {
            txtAllNodes = textView("All Nodes1:")

            fun View.myStyle() {
                lparams(wrapContent, wrapContent) {
                    leftMargin = 6
                    rightMargin = 6
                }
            }

            linearLayout {
                button("AddNode") {
                    onClick { addMyNode() }
                    myStyle()
                }

                button("PubMsg") {
                    onClick { app.connector.publish("/demo", ConvertMsgText.toBundle(MsgText("##xxx"))) }
                    myStyle()
                }
            }

            scrollView {
                txtInfo = textView("")
            }
//            verticalLayout {
//                lparams(matchParent, matchParent)
//                id = android.R.id.home
//            }
        }
    }.view

    protected var k = 0

    var txtInfo: TextView? = null
    fun showText(str: String) {
        runOnUiThread { txtInfo?.text = "${txtInfo?.text}\n$str" }
    }

    fun addMyNode() {
        val node = SimpleNode("Node_${k}", app.connector) {
            //
            onMessage { topic: String, data: Bundle ->
                showText("${name} recv: ${ConvertMsgText.toMsg(data)?.text}")
            }
            //
            onRegister {
                subscribe("/demo")
            }
        }
        node.register()
        k++
        //
        mgrNode.listNodes {
            txtAllNodes?.text = "ALL:${it.toList()}"
        }
    }

    override fun addCustomNodes() {
        logd("")
    }

    override fun initMaiFragment() {
        logd("")
    }
}