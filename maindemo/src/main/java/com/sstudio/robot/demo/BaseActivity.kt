package com.sstudio.robot.demo

import ai.easy.robot.user.RobotApplication
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.easy.robot.framework.MasterControlNode
import com.easy.robot.framework.MasterService

abstract class BaseActivity : Activity() {
    val app: RobotApplication by lazy { application as RobotApplication }

    val mgrNode: MasterControlNode by lazy {
        MasterControlNode(app.connector).apply {
            register()
        }
    }

    protected var txtAllNodes: TextView? = null

    abstract fun createView(): View?
    abstract fun addCustomNodes()
    abstract fun initMaiFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        //
        startService(Intent().apply { setClass(applicationContext, MasterService.javaClass) })

        super.onCreate(savedInstanceState)

        createView()?.apply { setContentView(this) }

        app.initMasterConnector(this) { isFirst ->

            if (isFirst) {
                mgrNode
                addCustomNodes()
            }
            //
            //fragmentManager.beginTransaction().add(R.id.home, TestTTSFragment()).commit()
            //
            mgrNode.listNodes {
                txtAllNodes?.text = "All1: ${it.toList()}"
            }
        }
    }


    override fun onDestroy() {
        //
        mgrNode.clearAll()
        app.releaseMasterConnector()

        super.onDestroy()
    }

}